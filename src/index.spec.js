const { expect } = require('chai');
const helloPackage = require('./index');

describe('Hello package', () => {
  it('should say hello world', () => {
    const resp = helloPackage();
    expect(resp).to.equal('Hello world !');
  });

  it('should say hello to somebody', () => {
    const resp = helloPackage('Jean-Michel');
    expect(resp).to.equal('Hello Jean-Michel !');
  });
});
