/**
 * Say hello to somebody
 * @param {string} name - somebody's name.
 * @returns {string}
 */
module.exports = (name = 'world') => {
  return `Hello ${name} !`;
};
