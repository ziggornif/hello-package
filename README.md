# hello-package

Gitlab package feature test.

## Usage

Update your project .npmrc with the following line :

```
@ziggornif:registry=https://gitlab.com/api/v4/packages/npm/
```

And add the package :

```sh
npm add @ziggornif/hello-package
```

See complete example in [examples/use-module](examples/use-module) directory.

## Project setup

```sh
npm ci
```

## Run tests

```sh
npm run test
```

## Lint files

```sh
npm run lint
```


<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>